* TODO Autotools
** DONE convert test.scm to a module                           :in progress:
** DONE Install scheme files to guile site
** TODO Test autotools in clean environment
* TODO Page search
* TODO Keybindings and UX
** TODO Todo Handle Key events inside Nomad not Emacs   :partially done:

- maps
key map is a list of pairs. the cons of the pair is the key and the
cdr is the function it maps too. A key cdr can also have the value of
a keymap this allows for prefix keys. not sure how to integrate this
into the events yet. This is loosely modelled after Emacs.

- states
vi bindings are not quite subject to prefix keys. and maps change
based on what mode the vi is in. We can have one state that is Emacs
and maps that handle states for other binding styles.

** TODO Create simple UI for input and pop-up display
** TODO Revisit this task, add support for prefix keys, and vim like states
** TODO keybind to copy current url to clipboard
* TODO Use GTK application classes

Classes created for application and window.

** WebKit View

Maybe turn this into a class?

** DONE Vte Terminal

widget to hold Emacs ncurses.

** DONE Decrease the size of vte widget
* TODO DOM link hinting

The DOM is not accessible from WebKit. It is single threaded and runs in it's own
process. In order to access it we need to do use RPC.

1. Javascript injection
2. Web extension

see [[https://wiki.gnome.org/Projects/WebKitGtk/ProgrammingGuide/Cookbook#Dealing_with_DOM_Tree][WebKit Cookbook]]

Probably our only option is to use a web extention. If we use
javascript we'll have to provide another layer of RPC anyways

** Prototype

Create a web extention that spawns a unix socket. Clients connect and
send a single line that is an s-expression one line is read and then
evaluated. Results are returned as an s-expression?
** TODO Server and repl modules

The server and repl modules, are redundant. Once we figure out what if
we need to use the webextention remove what we dont't neet.
* TODO Fancy html startup page?
* TODO Emacs integration
* TODO Minibuf history
* TODO Buffer list aka Tabs
